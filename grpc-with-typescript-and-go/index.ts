import {GetLogsRequest, GetLogsResponse} from './generated/service_pb';
import {LogServiceClient, ServiceError} from './generated/service_pb_service';

let root = document.getElementById('root');

let client = new LogServiceClient('http://localhost:9999'),
    request = new GetLogsRequest();
request.setPodname('pod/random-logger-7f68f7949-88zrw');

client.getLogs(request, (err: ServiceError | null, logs: GetLogsResponse | null) => {
    if (!root) {
        throw new Error('Failed to find root');
    }
    if (err) {
        root.innerHTML = 'Error: ' + err.message;
        return;
    }
    if (!logs) {
        root.innerHTML = 'No logs';
        return;
    }

    logs.getDataList().map(log => {
        if (!root) {
            throw new Error('Failed to find root');
        }
        let pEl = document.createElement('p');
        pEl.innerHTML = log;
        root.appendChild(pEl);
    });
});
