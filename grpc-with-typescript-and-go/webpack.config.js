const webpack = require('webpack');

module.exports = function makeWebpackConfig() {
    let config = {};

    config.entry = {
        index: "./index.ts",
    };

    config.devtool = 'eval-source-map';

    config.resolve = {
        extensions: [".ts", ".js"]
    };

    config.module = {
        rules: [
            {test: /\.ts?$/, loader: "awesome-typescript-loader"}
        ]
    };

    return config;
}();
