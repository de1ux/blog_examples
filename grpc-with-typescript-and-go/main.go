package main

import (
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"k8s.io/client-go/rest"
	"log"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	api "gitlab.com/de1ux/blog_examples/grpc-with-typescript-and-go/generated"
	"google.golang.org/grpc"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

type service struct{}

func getClientSet() (*kubernetes.Clientset, error) {
	config, err := clientcmd.BuildConfigFromFlags("", filepath.Join(homedir.HomeDir(), ".kube", "config"))
	if err != nil {
		return nil, err
	}

	return kubernetes.NewForConfig(config)
}

func getJobPodLogStream(client *kubernetes.Clientset, podName string) rest.Result {
	return client.
		RESTClient().
		Get().
		Prefix("api/v1").
		Namespace("default").
		Name(podName).
		Resource("pods").
		SubResource("log").
		Param("timestamps", "true").
		Do()
}

func (service) GetLogs(_ context.Context, request *api.GetLogsRequest) (*api.GetLogsResponse, error) {
	client, err := getClientSet()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to create clientset: %s", err)
	}

	result := getJobPodLogStream(client, request.PodName)
	if result.Error() != nil {
		return nil, status.Errorf(codes.Internal, "Failed to get logs: %s", result.Error())
	}

	b, err := result.Raw()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to coerce logs to bytes: %s", err)
	}

	lines := strings.Split(string(b), "\n")
	return &api.GetLogsResponse{Data: lines}, nil
}

func main() {
	grpcServer := grpc.NewServer()

	s := &service{}
	api.RegisterLogServiceServer(grpcServer, s)

	wrappedGrpcServer := grpcweb.WrapServer(grpcServer)

	log.Print("Accepting requests...")
	if err := (&http.Server{
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
				wrappedGrpcServer.ServeHTTP(w, r)
			} else {
				http.FileServer(http.Dir(".")).ServeHTTP(w, r)

			}
		}),
		Addr:    "0.0.0.0:9999",
	}).ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}